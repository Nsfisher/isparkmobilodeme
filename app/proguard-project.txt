##---------------Begin: proguard configuration common for Google Play Services----------------------
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
##---------------End: proguard configuration common for Google Play Services -----------------------
##--------------------------------------------------------------------------------------------------

##---------------Begin: proguard configuration common for Google Messaging Services ----------------
-keep class com.google.android.gms.**
-dontwarn com.google.android.gms.**
##---------------End: proguard configuration common for Google Messaging Services ------------------
##--------------------------------------------------------------------------------------------------

##---------------Begin: Remove Logging statements  -------------------------------------------------
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}
##---------------End: Remove Logging statements  ---------------------------------------------------
##--------------------------------------------------------------------------------------------------

##---------------Begin: proguard configuration for Gson  -------------------------------------------
-keep class com.google.gson.** { *; }
-keepattributes Signature
##---------------End: proguard configuration for Gson  ---------------------------------------------
##--------------------------------------------------------------------------------------------------

##---------------Begin: proguard configuration for Wasp --------------------------------------------
-keepattributes *Annotation*
-keep class com.orhanobut.wasp.** { *; }
-keepclassmembernames interface * {
    @com.orhanobut.wasp.http.* <methods>;
}
##---------------End: proguard configuration for Wasp ----------------------------------------------
##--------------------------------------------------------------------------------------------------

##---------------Begin: proguard configuration for OkHttp ------------------------------------------
-dontwarn com.squareup.okhttp.**
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
##---------------End: proguard configuration for OkHttp --------------------------------------------
##--------------------------------------------------------------------------------------------------


##---------------Begin: keep enum classes for serializations ---------------------------------------
-keep public enum com.verisun.mobiett.odeme.model.** {
    public *;
}

-keep class com.hoho.android.usbserial.** {*; }
##---------------End: keep enum classes for serializations -----------------------------------------
##--------------------------------------------------------------------------------------------------