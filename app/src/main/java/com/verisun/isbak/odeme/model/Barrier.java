package com.verisun.isbak.odeme.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/* RESPONSE JSON DATA
{
  "device": {
    "createDate": "09.05.2016 17:15:26",
    "token": "caf335fb8ca72aa55705ae29053ec97f91017d96d482295a6eb69672e9b6d35f",
    "type": "IPHONE",
    "id": "1",
    "status": "ACTIVE"
  },
  "loginDate": "09.05.2016 17:19:59",
  "loginStatus": "TRUE",
  "id": "1",
  "status": "ACTIVE"
} */
public class Barrier {

    @SerializedName("device")
    public Device device;


    @SerializedName("loginStatus")
    public String loginStatus;

    @SerializedName("id")
    public String id;

    @SerializedName("status")
    public String status;
    @SerializedName("doorNo")
    public String doorNo;

    public String getDoorNo() {
        return doorNo;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Barrier{" +
                ", device =" + device + '\'' +
                ", loginStatus ='" + loginStatus + '\'' +
                ", id ='"  + id + '\'' +
                ", status =" +status +
                '}';
    }
}
