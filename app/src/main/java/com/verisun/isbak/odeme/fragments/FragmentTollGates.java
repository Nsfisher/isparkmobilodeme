package com.verisun.isbak.odeme.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;

import com.verisun.isbak.odeme.R;
import com.verisun.isbak.odeme.core.BaseFragment;
/**
 * Created by nsfiSher on 28/04/16.
 */
public class FragmentTollGates extends BaseFragment implements View.OnClickListener {

    private TollSelectionListener listener;

    @Override
    protected void findViews() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tollgates;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        findViewById(R.id.otopark_giris).setOnClickListener(this);
        findViewById(R.id.otopark_cikis).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onTollSelected(v.getId());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TollSelectionListener) {
            listener = (TollSelectionListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface TollSelectionListener {
        void onTollSelected(@IdRes int tollId);
    }
}
