package com.verisun.isbak.odeme.core;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.wasp.Wasp;
import com.orhanobut.wasp.utils.AuthToken;
import com.orhanobut.wasp.utils.LogLevel;
import com.orhanobut.wasp.utils.RequestInterceptor;
import com.orhanobut.wasp.utils.WaspRetryPolicy;
import com.verisun.isbak.odeme.BuildConfig;

import io.fabric.sdk.android.Fabric;
import java.util.Map;

public class App extends Application {

    private static final String BaseURL = "https://78.186.131.245:8443/ispark.mobilepayment/rest";
    public static App instance;
    private Service service;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Logger.init(this);
        instance = this;

        service = new Wasp.Builder(this)
                .setEndpoint(BaseURL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void onHeadersAdded(Map<String, String> stringStringMap) {
                    }

                    @Override
                    public void onQueryParamsAdded(Map<String, Object> stringObjectMap) {

                    }

                    @Override
                    public WaspRetryPolicy getRetryPolicy() {
                        return null;
                    }

                    @Override
                    public AuthToken getAuthToken() {
                        return new AuthToken("Basic YW5kcm9pZDpuVGFmUXhWRDZNZnBHS1hvbkVxQQ==");
                    }
                })
                .trustCertificates()
                .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
                .build()
                .create(Service.class);
    }

    public Service getService() {
        return service;
    }
}
