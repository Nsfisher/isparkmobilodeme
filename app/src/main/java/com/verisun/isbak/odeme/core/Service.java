package com.verisun.isbak.odeme.core;

import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.http.Body;
import com.orhanobut.wasp.http.GET;
import com.orhanobut.wasp.http.POST;
import com.orhanobut.wasp.http.PUT;
import com.orhanobut.wasp.http.Path;
import com.verisun.isbak.odeme.model.Barrier;

import java.util.ArrayList;
/**
 * Created by nsfiSher on 23/04/16.
 */
public interface Service {

    @GET("/login/loginCheck")
    void getLoginCheck(Callback<Barrier> callBack);

    @GET("/login/logoutCheck")
    void getLogoutCheck(Callback<Barrier> callBack);


    @PUT("/login/{deviceId}/barrierControl/?doorNo={doorNo}")
    void setLoginSuccess(@Path("deviceId") String deviceId,@Path("doorNo") String doorNo,@Body Callback<Barrier> callback);


}

