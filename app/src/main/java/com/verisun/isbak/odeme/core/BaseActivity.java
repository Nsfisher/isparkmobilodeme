package com.verisun.isbak.odeme.core;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.verisun.isbak.odeme.R;
/**
 * Created by nsfiSher on 27/04/16.
 */
public abstract class BaseActivity extends ActionBarActivity {

    protected final String TAG =getClass().getSimpleName();;
    protected BaseActivity mActivity;
    protected App mApplication;
    protected FragmentManager mFragmentManager;
    protected Toolbar toolbar;

    public BaseActivity() {
        mActivity = this;
        mFragmentManager = getFragmentManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (App) getApplication();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
           // getSupportActionBar().setLogo(R.drawable.logo);
        }
    }

    @Override
    public void onBackPressed() {
        int cnt = mFragmentManager.getBackStackEntryCount();
        if (cnt != 0) {
            mFragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public Service getService() {
        return mApplication.getService();
    }
}
