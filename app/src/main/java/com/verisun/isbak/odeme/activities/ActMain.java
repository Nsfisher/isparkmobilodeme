package com.verisun.isbak.odeme.activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.verisun.isbak.odeme.R;
import com.verisun.isbak.odeme.core.Logger;
import com.verisun.isbak.odeme.fragments.FragmentLogin;
import com.verisun.isbak.odeme.fragments.FragmentTollGates;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by nsfipher on 27/04/16.
 */
public class ActMain extends BaseMain implements FragmentLogin.PaymentReceivedListener, SerialInputOutputManager.Listener {

    private static final String TAG = "USB";
    private static final int USB_WRITE_TIMEOUT_MILLIS = 10 * 1000;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private UsbManager usbManager;
    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Logger.i(TAG, "receiver onReceive action = " + action);
            checkIntent(intent);
        }
    };
    private UsbSerialPort usbSerialPort;
    private SerialInputOutputManager serialInputOutputManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideBars();
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        // listen for new devices
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);
    }

    @Override
    public void onBackPressed() {
        mFragmentManager.beginTransaction()
                .replace(R.id.container, new FragmentTollGates())
                .commit();
        TextView status=(TextView)findViewById(R.id.status);
        status.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String action = intent.getAction();
        Logger.i(TAG, "onResume action: " + action);
        checkIntent(intent);
    }

    private void checkIntent(Intent intent) {
        String action = intent.getAction();

        if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            Logger.i(TAG, "device attached");
            UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            setDevice(device);
        } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
            Logger.i(TAG, "device detached");
            setDevice(null);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mUsbReceiver);
        closePort();
        stopIoManager();
        super.onDestroy();
    }

    protected void notifyUsb() {
        Logger.i(TAG, "notifying USB");
        if (usbSerialPort != null) {
            try {
                usbSerialPort.write(HexDump.hexStringToByteArray("02ff9605112b353403"), USB_WRITE_TIMEOUT_MILLIS);
                Logger.i(TAG, "usb notified");
            } catch (Exception e) {
                Logger.e(TAG, "can't notify USB. " + e.getMessage(), e);
                Logger.e(TAG, e.getMessage(), e);
//                Toast.makeText(mActivity, "can't write data to port:" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Logger.i(TAG, "can't notify USB. there aren't any device connected ");
//            Toast.makeText(mActivity, "there is no usb device attached", Toast.LENGTH_SHORT).show();
        }
    }

    private void setDevice(UsbDevice device) {
        if (device != null) {
            UsbDeviceConnection connection = usbManager.openDevice(device);
            if (connection != null) {
            //    Toast.makeText(getApplicationContext(),"connected",Toast.LENGTH_SHORT).show();
                Logger.i(TAG, "connected to device");
                usbSerialPort = openPort(connection, device);
                startIoManager();
            } else {
                Logger.i(TAG, "can't connect to device");
            }
        } else {
//            Toast.makeText(mActivity, "there is no device attached. ", Toast.LENGTH_SHORT).show();
            Logger.i(TAG, "there is no device attached");
        }
    }

    private UsbSerialPort openPort(@NonNull UsbDeviceConnection connection, @NonNull UsbDevice device) {

        //Toast.makeText(getApplicationContext(),"openP",Toast.LENGTH_SHORT).show();
        UsbSerialPort port = UsbSerialProber.getDefaultProber().probeDevice(device).getPorts().get(0);
        try {
            port.open(connection);
            port.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
          //  Toast.makeText(getApplicationContext(),"successfully",Toast.LENGTH_SHORT).show();
            Logger.i(TAG, "port successfully opened");
            return port;
        } catch (IOException e) {
            Logger.e(TAG, "opening port failed :" + e.getMessage(), e);
            try {
             //   Toast.makeText(getApplicationContext(),"closePort",Toast.LENGTH_SHORT).show();
                port.close();
            } catch (IOException ignored) {
            }
        }
        return null;
    }

    private void closePort() {
        Logger.i(TAG, "closing port");
        if (usbSerialPort != null) {
            try {
            //    Toast.makeText(getApplicationContext(),"closeP",Toast.LENGTH_SHORT).show();

                usbSerialPort.close();
            } catch (IOException ignored) {
            }
            usbSerialPort = null;
        }
    }

    private void startIoManager() {
        if (usbSerialPort != null) {
            Logger.i(TAG, "Starting io manager ..");
          //  Toast.makeText(getApplicationContext(),"startIO",Toast.LENGTH_SHORT).show();
            serialInputOutputManager = new SerialInputOutputManager(usbSerialPort, this);
            executorService.submit(serialInputOutputManager);
        }
    }

    private void stopIoManager() {
        if (serialInputOutputManager != null) {
            Logger.i(TAG, "Stopping io manager ..");

           // Toast.makeText(getApplicationContext(),"stopIO",Toast.LENGTH_SHORT).show();
            serialInputOutputManager.stop();
            serialInputOutputManager = null;
        }
    }

    @Override
    public void onRunError(Exception e) {
        stopIoManager();
        startIoManager();
    }

    @Override
    public void onNewData(final byte[] data) {
        Logger.i(TAG, "new Data received : " + new String(data));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void hideBars() {

        int visibility = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            visibility = visibility | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(visibility);
    }
}
