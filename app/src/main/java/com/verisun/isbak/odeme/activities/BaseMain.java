package com.verisun.isbak.odeme.activities;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.Response;
import com.orhanobut.wasp.WaspError;
import com.verisun.isbak.odeme.R;
import com.verisun.isbak.odeme.core.BaseActivity;
import com.verisun.isbak.odeme.core.Logger;
import com.verisun.isbak.odeme.core.Service;
import com.verisun.isbak.odeme.fragments.FragmentLogin;
import com.verisun.isbak.odeme.fragments.FragmentTollGates;
import com.verisun.isbak.odeme.model.Barrier;

/**
 * Created by nsfiSher on 27/04/16.
 */
public abstract class BaseMain extends BaseActivity implements FragmentLogin.PaymentReceivedListener,FragmentTollGates.TollSelectionListener {

    private static final String PREF_TOLL_GATE_DOOR_NO = "preferences_toll_gate_door_no";
    private static final String PREF_TOLL_GATE_TITLE = "preferences_toll_gate_title";

    private LinearLayout newPaymentLayout;
    private TextView infoPass,status;
    protected View progress;
    protected ImageView animationLayout;
    protected AnimationDrawable paymentTickAnimation;
    private FrameLayout inContainer;
    private Toolbar mTollbar;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        String doorNo = getPreferences(MODE_PRIVATE).getString(PREF_TOLL_GATE_DOOR_NO, null);
        String title = getPreferences(MODE_PRIVATE).getString(PREF_TOLL_GATE_TITLE, null);
        status = (TextView) findViewById(R.id.status);
        if (doorNo != null) {
            setTollGate(title, doorNo,true);
            if (doorNo.equals("00001")) {
                status.setText(getResources().getString(R.string.otopark_giris_screen));
                status.setVisibility(View.VISIBLE);
            } else {
                status.setText(getResources().getString(R.string.otopark_cikis_screen));
                status.setVisibility(View.VISIBLE);
            }
        } else {

            mFragmentManager.beginTransaction()
                    .replace(R.id.container, new FragmentTollGates())
                    .commit();
        }
        newPaymentLayout = (LinearLayout) findViewById(R.id.new_payment_layout);
       paymentTickAnimation = (AnimationDrawable) ((ImageView) findViewById(R.id.payment_tick)).getDrawable();
        infoPass = (TextView) findViewById(R.id.pass_information);
        //progress = findViewById(R.id.progress);
        mediaPlayer = MediaPlayer.create(mActivity, R.raw.tam);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onTollSelected(@IdRes int tollId) {
        switch (tollId) {
            case R.id.otopark_giris:
                setTollGate(getString(R.string.otopark_giris), "00001",false);
                break;
            case R.id.otopark_cikis:
                setTollGate(getString(R.string.otopark_cikis), "00002",false);
                break;

        }
    }

    private void setTollGate(@Nullable String title, @NonNull String doorNo,boolean value) {
        getPreferences(MODE_PRIVATE)
                .edit()
                .putString(PREF_TOLL_GATE_TITLE, title)
                .putString(PREF_TOLL_GATE_DOOR_NO, doorNo)
                .apply();

        if (!value) {
            FrameLayout frameLayout=(FrameLayout)findViewById(R.id.container);
            Button giris = (Button) frameLayout.findViewById(R.id.otopark_giris);
            Button cikis = (Button) frameLayout.findViewById(R.id.otopark_cikis);
            giris.setVisibility(View.GONE);
            cikis.setVisibility(View.GONE);


            if (title.equals(getResources().getString(R.string.otopark_cikis))) {
                status.setText(getResources().getString(R.string.otopark_cikis_screen));
                status.setVisibility(View.VISIBLE);
            } else {
                status.setText(getResources().getString(R.string.otopark_giris_screen));
                status.setVisibility(View.VISIBLE);
            }
        }



            mFragmentManager.beginTransaction()
                    .replace(R.id.container, FragmentLogin.newInstance(title, doorNo))
                    .commit();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.add("");
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                try {
                    throw new RuntimeException("test");
                } catch (Exception e ) {
                    Crashlytics.logException(e);
                    Toast.makeText(mActivity, "log sent", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public void newPaymentReceived(@Nullable final Barrier payment) {
        notifyUsb();
        if (payment != null) {
            status.setVisibility(View.GONE);

            if (payment.doorNo.equals("1")) {
                infoPass.setText(getResources().getString(R.string.passplease));
            } else {
                infoPass.setText(getResources().getString(R.string.outplease));
            }

            newPaymentLayout.setVisibility(View.VISIBLE);
            mediaPlayer.start();

            new CountDownTimer(3000, 3000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }
                @Override
                public void onFinish() {
                    newPaymentLayout.setVisibility(View.GONE);
                    status.setVisibility(View.VISIBLE);
                    setLogin(payment.device.id,payment.doorNo);
                }
            }.start();
        }
    }
    public void  setLogin(String  deviceID,String doorNo) {
        Service service=getService();
        if (service != null) {
            service.setLoginSuccess(deviceID,doorNo,new Callback<Barrier>() {

                @Override
                public void onSuccess(Response response, Barrier s) {
                    Log.e("Open:","Open Barrier");
                }
                @Override
                public void onError(WaspError waspError) {
                    Logger.i("Barrier", "error =" + waspError.getErrorMessage());

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mFragmentManager.beginTransaction()
                .replace(R.id.container, new FragmentTollGates())
                .commit();

    }

    protected abstract void notifyUsb();

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
