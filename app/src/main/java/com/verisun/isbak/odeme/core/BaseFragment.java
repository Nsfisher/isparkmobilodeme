package com.verisun.isbak.odeme.core;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * Created by nsfiSher on 27/04/16.
 */
public abstract class BaseFragment extends Fragment {

    protected BaseActivity mActivity;
    protected App mApplication;
    protected View mRootView;
    protected Bundle mArguments;
    protected LayoutInflater mInflater;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mArguments = getArguments();
        if (mArguments == null) {
            mArguments = new Bundle();
        }
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mInflater = inflater;
        mRootView = inflater.inflate(getLayoutId(), container, false);
        findViews();
        init(savedInstanceState);

        return mRootView;
    }

    protected abstract void findViews();

    protected abstract int getLayoutId();

    protected abstract void init(Bundle savedInstanceState);

    protected final View findViewById(int id) {
        return mRootView.findViewById(id);
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = (BaseActivity) activity;
        mApplication = (App) mActivity.getApplication();
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public final Service getService() {
        if (mActivity != null) {
            return mActivity.getService();
        }
        return null;
    }
}
