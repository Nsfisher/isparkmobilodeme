package com.verisun.isbak.odeme.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.verisun.isbak.odeme.R;
import com.verisun.isbak.odeme.core.BaseActivity;
/**
 * Created by nsfiSher on 30/04/16.
 */
public class ActSplash extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) &&
                    intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
            }
        } else {
            hideBars();
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.isparklogo);
            imageView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(mActivity, ActMain.class));
                }
            }, 3000);
            setContentView(imageView);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void hideBars() {

        int visibility = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            visibility = visibility | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(visibility);
    }
}
