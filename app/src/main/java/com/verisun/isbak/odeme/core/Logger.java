package com.verisun.isbak.odeme.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.verisun.isbak.odeme.BuildConfig;

import io.fabric.sdk.android.Fabric;


public class Logger implements Application.ActivityLifecycleCallbacks {

    public static void init(Context context) {
        if (!BuildConfig.DEBUG) {
            Fabric.with(context, new Crashlytics());
        }
        ((App) context.getApplicationContext()).registerActivityLifecycleCallbacks(new Logger());
    }

    public static void logKeyValue(String key, String value) {
        if (Fabric.isInitialized()) {
            log(key + "=" + value);
            Crashlytics.setString(key, value);
        }
    }

    public static void log(String message) {
        d("Logger", message);
    }

    public static void d(String tag, String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.d(tag, message);
    }

    public static void i(String tag, String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.i(tag, message);

    }

    public static void w(String tag, String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.w(tag, message);

    }

    public static void e(String tag, String message, Throwable e) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message + "\n stacktrace=" + Log.getStackTraceString(e));
            Crashlytics.logException(e);
        }
        Log.e(tag, message, e);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        log("activity created = " + activity.getClass().getSimpleName());
        logKeyValue("activityCreated", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        log("activity started = " + activity.getClass().getSimpleName());
        logKeyValue("activityStarted", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        log("activity resumed = " + activity.getClass().getSimpleName());
        logKeyValue("activityResumed", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityPaused(Activity activity) {
        log("activity paused = " + activity.getClass().getSimpleName());
        logKeyValue("activityPaused", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        log("activity stopped = " + activity.getClass().getSimpleName());
        logKeyValue("activityStopped", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        log("activity saveInstanceState = " + activity.getClass().getSimpleName());
        logKeyValue("activitySaveInstanceState", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        log("activity destroyed = " + activity.getClass().getSimpleName());
        logKeyValue("activityDestroyed", activity.getClass().getSimpleName());
    }
}
