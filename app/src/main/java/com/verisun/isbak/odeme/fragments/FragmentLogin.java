package com.verisun.isbak.odeme.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.Response;
import com.orhanobut.wasp.WaspError;
import com.verisun.isbak.odeme.R;
import com.verisun.isbak.odeme.core.BaseFragment;
import com.verisun.isbak.odeme.core.Logger;
import com.verisun.isbak.odeme.core.Service;
import com.verisun.isbak.odeme.model.Barrier;


import java.util.Timer;
import java.util.TimerTask;
/**
 * Created by nsfiSher on 27/04/16.
 */
public class FragmentLogin extends BaseFragment {

    private ListView listView;
    private ArrayAdapter<Barrier> adapter;

    private Barrier lastPayment;
    private PaymentReceivedListener listener;

    private Timer timer;
    private String doorNo;
    int count=0;
    public static FragmentLogin newInstance(String title, @NonNull String doorNo) {
        FragmentLogin fragmentLogin = new FragmentLogin();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("doorNo", doorNo);
        fragmentLogin.setArguments(args);
        return fragmentLogin;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PaymentReceivedListener) {
            listener = (PaymentReceivedListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void findViews() {
        listView = (ListView) findViewById(R.id.payments_list);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_payments;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        //listView.setAdapter(adapter);

        String title = getArguments().getString("title");
        ((TextView) findViewById(R.id.payments_title)).setText(title);

        String doorNo = getArguments().getString("doorNo");
        setDoorNo(doorNo);
    }

    private void setDoorNo(@NonNull final String doorNo) {
        if (timer != null) {
            timer.cancel();
        }
        lastPayment = null;
        this.doorNo = doorNo;
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (doorNo.equals("00001")) {
                    checkLogin();
                } else {
                    checkLogout();
                }
            }
        }, 0, 1000);

    }

    private void checkLogin() {
        Service serviceMobiettOdeme = getService();
        if (doorNo != null && serviceMobiettOdeme != null) {

            serviceMobiettOdeme.getLoginCheck(new Callback<Barrier>() {
                @Override
                public void onSuccess(Response response, Barrier barriers) {
                    if (barriers != null ) {
                        count++;
                        if (count == 1) {
                            listener.newPaymentReceived(barriers);
                        }

                    } else {
                        count = 0;
                        Logger.i("Barrier", "success payments are null!!!");
                    }
                }

                @Override
                public void onError(WaspError waspError) {
                    Logger.i("Barrier", "error =" + waspError.getErrorMessage());

                }
            });

        }
    }

    private void checkLogout() {

        Service serviceMobiettOdeme = getService();
        if (doorNo != null && serviceMobiettOdeme != null) {

            serviceMobiettOdeme.getLogoutCheck(new Callback<Barrier>() {
                @Override
                public void onSuccess(Response response, Barrier barriers) {

                    if (barriers != null ) {
                        count++;
                        if (count==1) {
                            listener.newPaymentReceived(barriers);
                            Log.e("Close", "Close Barrier");
                        }

                    } else {
                        count = 0;
                        Logger.i("Barrier", "success payments are null!!!");
                    }
                }
                @Override
                public void onError(WaspError waspError) {
                    Logger.i("Barrier", "error =" + waspError.getErrorMessage());

                }
            });

        }
    }

    public interface PaymentReceivedListener {
        void newPaymentReceived(Barrier payment);
    }
}
