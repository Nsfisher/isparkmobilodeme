package com.verisun.isbak.odeme.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.widget.Toast;

//import com.jakewharton.processphoenix.ProcessPhoenix;
/**
 * Created by nsfiSher on 26/04/16.
 */
public class BootCompletedReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(final Context context, Intent intent) {
        new CountDownTimer(2000, 2000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //ProcessPhoenix.triggerRebirth(context.getApplicationContext());
            }
        }.start();
    }
}