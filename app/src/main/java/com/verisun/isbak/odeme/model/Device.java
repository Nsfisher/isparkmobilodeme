package com.verisun.isbak.odeme.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by nsfiSher on 29/04/16.
 */
 /*
"device": {
        "createDate": "09.05.2016 17:15:26",
        "token": "caf335fb8ca72aa55705ae29053ec97f91017d96d482295a6eb69672e9b6d35f",
        "type": "IPHONE",
        "id": "1",
        "status": "ACTIVE"
        }
  */
public class Device {


    @SerializedName("token")
    public String token;

    @SerializedName("type")
    public String type;

    @SerializedName("id")
    public String id;

    @SerializedName("status")
    public  String status;

    public String getToken() {
        return token;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }
}
